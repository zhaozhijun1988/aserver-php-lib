<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;
use \Mojomaja\Component\Aserv\User;

class LoginTest extends PHPUnit_Framework_TestCase
{
    public function testLoginOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/login'),
                $this->equalTo([
                    'ident'     => 'one@example.com',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                'user'  => [
                    'id'        => 113,
                    'name'      => null,
                    'mobile'    => null,
                    'email'     => 'one@example.com',
                    'nickname'  => 'anony.',
                    'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg'
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->login('one@example.com', 'secret');
        $this->assertEquals('v-h0WvJREeOf7wgAJyXQOYoClqM=', $aserv->getToken());
        $this->assertEquals(113, $user->getId());
        $this->assertNull($user->getName());
        $this->assertNull($user->getMobile());
        $this->assertEquals('one@example.com', $user->getEmail());
        $this->assertEquals('anony.', $user->getNickname());
        $this->assertEquals('da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg', $user->getPortrait());
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_MISSING_ARGUMENT
     */
    public function testLoginMissingArgument()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/login'),
                $this->equalTo([
                    'ident'     => null,
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_MISSING_ARGUMENT,
                'message'   => 'missing argument',
                'cause'     => ['ident']
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->login(null, 'secret');
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_BAD_CREDENTIAL
     */
    public function testLoginBadCredential()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/login'),
                $this->equalTo([
                    'ident'     => 'one@example.com',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_BAD_CREDENTIAL,
                'message'   => 'bad credential',
                'cause'     => []
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->login('one@example.com', 'secret');
    }
}
