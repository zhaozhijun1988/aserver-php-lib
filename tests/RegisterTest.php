<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;
use \Mojomaja\Component\Aserv\User;

class RegisterTest extends PHPUnit_Framework_TestCase
{
    public function testRegisterOk()
    {
        $created_at     = new \DateTime('2014-08-21T15:27:37.797705');
        $activated_at   = new \DateTime('2014-08-21T15:38:49.628597');
        $skurl          = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/register'),
                $this->equalTo([
                    'email'     => 'one@example.com',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69',
                    'nickname'  => 'anony.',
                    'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'active'    => 'true'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                'user'  => [
                    'id'            => 113,
                    'name'          => null,
                    'mobile'        => null,
                    'email'         => 'one@example.com',
                    'nickname'      => 'anony.',
                    'portrait'      => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'created_at'    => '2014-08-21T15:27:37.797705',
                    'activated_at'  => '2014-08-21T15:38:49.628597'
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->register(new User([
            'email'     => 'one@example.com',
            'nickname'  => 'anony.',
            'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg'
        ]), 'secret');
        $this->assertEquals('v-h0WvJREeOf7wgAJyXQOYoClqM=', $aserv->getToken());
        $this->assertEquals(113, $user->getId());
        $this->assertNull($user->getName());
        $this->assertNull($user->getMobile());
        $this->assertEquals('one@example.com', $user->getEmail());
        $this->assertEquals('anony.', $user->getNickname());
        $this->assertEquals('da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg', $user->getPortrait());
        $this->assertEquals($created_at, $user->getCreatedAt());
        $this->assertEquals($activated_at, $user->getActivatedAt());
    }

    public function testRegisterInactiveOk()
    {
        $created_at     = new \DateTime('2014-08-21T15:27:37.797705');
        $skurl          = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/register'),
                $this->equalTo([
                    'email'     => 'one@example.com',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69',
                    'nickname'  => 'anony.',
                    'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'active'    => 'false'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                'user'  => [
                    'id'            => 113,
                    'name'          => null,
                    'mobile'        => null,
                    'email'         => 'one@example.com',
                    'nickname'      => 'anony.',
                    'portrait'      => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'created_at'    => '2014-08-21T15:27:37.797705',
                    'activated_at'  => null
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->register(new User([
            'email'     => 'one@example.com',
            'nickname'  => 'anony.',
            'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg'
        ]), 'secret', false);
        $this->assertEquals('v-h0WvJREeOf7wgAJyXQOYoClqM=', $aserv->getToken());
        $this->assertEquals(113, $user->getId());
        $this->assertNull($user->getName());
        $this->assertNull($user->getMobile());
        $this->assertEquals('one@example.com', $user->getEmail());
        $this->assertEquals('anony.', $user->getNickname());
        $this->assertEquals('da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg', $user->getPortrait());
        $this->assertEquals($created_at, $user->getCreatedAt());
        $this->assertNull($user->getActivatedAt());
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_MISSING_ARGUMENT
     */
    public function testRegisterMissingArgument()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/register'),
                $this->equalTo([
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69',
                    'active'    => 'true'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_MISSING_ARGUMENT,
                'message'   => 'missing argument',
                'cause'     => ['ident']
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->register(new User(), 'secret');
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_DUPLICATE_IDENT
     */
    public function testRegisterDupIdent()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/register'),
                $this->equalTo([
                    'email'     => 'one@example.com',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69',
                    'active'    => 'true'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_DUPLICATE_IDENT,
                'message'   => 'duplicate ident',
                'cause'     => ['email']
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->register(new User(['email' => 'one@example.com']), 'secret');
    }
}
