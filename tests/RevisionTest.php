<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;

class RevisionTest extends PHPUnit_Framework_TestCase
{
    public function testRevision()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('http://example.com/'))
            ->will($this->returnValue('{"revision": "14bd3811a43a475c1728b22b3f069ab5aae7b4e6"}'))
        ;

        $aserv = new Client('http://example.com', $skurl);
        $this->assertEquals('14bd3811a43a475c1728b22b3f069ab5aae7b4e6', $aserv->revision());
    }

    /**
     * @expectedException Mojomaja\Component\Aserv\JsonException
     */
    public function testNotJson()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('http://example.com/'))
            ->will($this->returnValue('not json'))
        ;

        $aserv = new Client('http://example.com', $skurl);
        $aserv->revision();
    }
}
