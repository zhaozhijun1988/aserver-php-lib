<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;
use \Mojomaja\Component\Aserv\User;

class ProfileTest extends PHPUnit_Framework_TestCase
{
    public function testGetProfileOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('http://example.com/v1/profile'),
                $this->equalTo(['token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM='])
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'user'  => [
                    'id'        => 113,
                    'name'      => null,
                    'mobile'    => '18901234567',
                    'email'     => 'one@example.com',
                    'nickname'  => 'one',
                    'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'gender'    => 'female'
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->profile();
        $this->assertEquals('v-h0WvJREeOf7wgAJyXQOYoClqM=', $aserv->getToken());
        $this->assertEquals(113, $user->getId());
        $this->assertNull($user->getName());
        $this->assertEquals('18901234567', $user->getMobile());
        $this->assertEquals('one@example.com', $user->getEmail());
        $this->assertEquals('one', $user->getNickname());
        $this->assertEquals('da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg', $user->getPortrait());
        $this->assertEquals(User::FEMALE, $user->getGender());
    }

    public function testSetProfileOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('put')
            ->with(
                $this->equalTo('http://example.com/v1/profile'),
                $this->equalTo([
                    'token'     => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'email'     => 'one@example.net',
                    'nickname'  => 'anony.',
                    'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'gender'    => 'male'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'user'  => [
                    'id'        => 113,
                    'name'      => null,
                    'mobile'    => '18901234567',
                    'email'     => 'one@example.net',
                    'nickname'  => 'anony.',
                    'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'gender'    => 'male'
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user
            = $aserv
            ->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')
            ->flush(new User([
                'email'     => 'one@example.net',
                'nickname'  => 'anony.',
                'portrait'  => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                'gender'    => User::MALE
            ]))
        ;
        $this->assertEquals('v-h0WvJREeOf7wgAJyXQOYoClqM=', $aserv->getToken());
        $this->assertEquals(113, $user->getId());
        $this->assertNull($user->getName());
        $this->assertEquals('18901234567', $user->getMobile());
        $this->assertEquals('one@example.net', $user->getEmail());
        $this->assertEquals('anony.', $user->getNickname());
        $this->assertEquals('da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg', $user->getPortrait());
        $this->assertEquals(User::MALE, $user->getGender());
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_DUPLICATE_IDENT
     */
    public function testSetProfileDupIdent()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('put')
            ->with(
                $this->equalTo('http://example.com/v1/profile'),
                $this->equalTo([
                    'token'     => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'email'     => 'one@example.net'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_DUPLICATE_IDENT,
                'message'   => 'duplicate ident',
                'cause'     => ['email']
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user
            = $aserv
            ->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')
            ->flush(new User(['email' => 'one@example.net']))
        ;
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_FIXED_NAME
     */
    public function testSetProfileFixedName()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('put')
            ->with(
                $this->equalTo('http://example.com/v1/profile'),
                $this->equalTo([
                    'token'     => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'name'      => 'anon',
                    'email'     => 'one@example.net'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_FIXED_NAME,
                'message'   => 'fixed name',
                'cause'     => []
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user
            = $aserv
            ->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')
            ->flush(new User([
                'name'  => 'anon',
                'email' => 'one@example.net'
            ]))
        ;
    }
}
