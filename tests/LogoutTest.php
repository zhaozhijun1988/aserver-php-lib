<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;

class LogoutTest extends PHPUnit_Framework_TestCase
{
    public function testLogoutOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/logout'),
                $this->equalTo(['token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM='])
            )
            ->will($this->returnValue(json_encode(['error' => Client::E_OK])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->logout();
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_MISSING_ARGUMENT
     */
    public function testLogoutMissingArgument()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/logout'),
                $this->equalTo(['token' => null])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_MISSING_ARGUMENT,
                'message'   => 'missing argument',
                'cause'     => 'token'
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->logout();
    }

    /**
     * @expectedException       Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   Mojomaja\Component\Aserv\Client::E_BAD_TOKEN
     */
    public function testLogoutBadToken()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/logout'),
                $this->equalTo(['token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM='])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_BAD_TOKEN,
                'message'   => 'bad token',
                'cause'     => []
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->logout();
    }
}
