<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;
use \Mojomaja\Component\Aserv\Password;

class PasswordTest extends PHPUnit_Framework_TestCase
{
    public function testHashPasswordOk()
    {
        $this->assertEquals('5ebe2294ecd0e0f08eab7690d2a6ee69', new Password('secret'));
        $this->assertEquals(
            '5ebe2294ecd0e0f08eab7690d2a6ee69',
            new Password('5ebe2294ecd0e0f08eab7690d2a6ee69', true)
        );
        $this->assertEquals(
            '5ebe2294ecd0e0f08eab7690d2a6ee69',
            new Password(new Password('secret'))
        );
    }

    public function testCheckPasswordOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('http://example.com/v1/password'),
                $this->equalTo([
                    'token'     => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode(['error' => Client::E_OK])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->check('secret');
    }

    /**
     * @expectedException       \Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   \Mojomaja\Component\Aserv\Client::E_FALSE_PASSWORD
     */
    public function testCheckPasswordFalsePassword()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('http://example.com/v1/password'),
                $this->equalTo([
                    'token'     => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_FALSE_PASSWORD,
                'message'   => 'false password',
                'cause'     => []
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->check('secret');
    }

    public function testChangePasswordOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/password'),
                $this->equalTo([
                    'token'         => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'new_password'  => 'f1a0e73e5d7fc293952d380e32fda73c',
                    'old_password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode(['error' => Client::E_OK])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->password('himitsu', 'secret');
    }

    public function testResetPasswordOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('put')
            ->with(
                $this->equalTo('http://example.com/v1/password'),
                $this->equalTo([
                    'token'     => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                    'password'  => '5ebe2294ecd0e0f08eab7690d2a6ee69'
                ])
            )
            ->will($this->returnValue(json_encode(['error' => Client::E_OK])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $aserv->setToken('v-h0WvJREeOf7wgAJyXQOYoClqM=')->password('secret', true);
    }
}
