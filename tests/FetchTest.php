<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;

class FetchTest extends PHPUnit_Framework_TestCase
{
    public function testFetchOk()
    {
        $created_at     = new \DateTime('2014-08-21T15:27:37.797705');
        $activated_at   = new \DateTime('2014-08-21T15:38:49.628597');
        $skurl          = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('get')
            ->with(
                $this->equalTo('http://example.com/v1/fetch'),
                $this->equalTo('id=113&active=true')
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'user'  => [
                    [
                        'id'            => 113,
                        'name'          => null,
                        'mobile'        => '18901234567',
                        'email'         => 'one@example.com',
                        'created_at'    => '2014-08-21T15:27:37.797705',
                        'activated_at'  => '2014-08-21T15:38:49.628597'
                    ]
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->fetch([113]);
        $this->assertCount(1, $user);
        $this->assertEquals(113, $user[0]->getId());
        $this->assertNull($user[0]->getName());
        $this->assertEquals('18901234567', $user[0]->getMobile());
        $this->assertEquals('one@example.com', $user[0]->getEmail());
        $this->assertEquals($created_at, $user[0]->getCreatedAt());
        $this->assertEquals($activated_at, $user[0]->getActivatedAt());
    }
}
