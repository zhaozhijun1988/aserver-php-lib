<?php

require_once dirname(__file__).'/../vendor/autoload.php';

use \Mojomaja\Component\Aserv\Client;

class SubstituteTest extends PHPUnit_Framework_TestCase
{
    public function testSubstituteOk()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/substitute'),
                $this->equalTo(['ident' => 'one@example.com'])
            )
            ->will($this->returnValue(json_encode([
                'error' => Client::E_OK,
                'token' => 'v-h0WvJREeOf7wgAJyXQOYoClqM=',
                'user'  => [
                    'id'        => 113,
                    'name'      => null,
                    'mobile'    => null,
                    'email'     => 'one@example.com'
                ]
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->su('one@example.com');
        $this->assertEquals('v-h0WvJREeOf7wgAJyXQOYoClqM=', $aserv->getToken());
        $this->assertEquals(113, $user->getId());
        $this->assertNull($user->getName());
        $this->assertNull($user->getMobile());
        $this->assertEquals('one@example.com', $user->getEmail());
    }

    /**
     * @expectedException       \Mojomaja\Component\Aserv\Exception
     * @expectedExceptionCode   \Mojomaja\Component\Aserv\Client::E_FALSE_IDENT
     */
    public function testSubstituteFalseIdent()
    {
        $skurl = $this->getMock('Mojomaja\\Component\\Skurl\\Client');
        $skurl
            ->expects($this->once())
            ->method('post')
            ->with(
                $this->equalTo('http://example.com/v1/substitute'),
                $this->equalTo(['ident' => 'one@example.com'])
            )
            ->will($this->returnValue(json_encode([
                'error'     => Client::E_FALSE_IDENT,
                'message'   => 'false ident',
                'cause'     => []
            ])))
        ;

        $aserv  = new Client('http://example.com', $skurl);
        $user   = $aserv->su('one@example.com');
    }
}
