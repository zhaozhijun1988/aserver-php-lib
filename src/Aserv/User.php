<?php

namespace Mojomaja\Component\Aserv;

class User
{
    const FEMALE    = 'female';
    const MALE      = 'male';

    private $id;
    private $name;
    private $mobile;
    private $email;
    private $nickname;
    private $portrait;
    private $gender;
    private $created_at;
    private $activated_at;

    public function __construct(Array $u = [])
    {
        $this->join($u);
    }

    public function join(Array $u = [])
    {
        foreach ($u as $k => $v)
            $this->set($k, $v);

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getPortrait()
    {
        return $this->portrait;
    }

    public function setPortrait($portrait)
    {
        $this->portrait = $portrait;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at ? new \DateTime($created_at) : null;

        return $this;
    }

    public function getActivatedAt()
    {
        return $this->activated_at;
    }

    public function setActivatedAt($activated_at)
    {
        $this->activated_at = $activated_at ? new \DateTime($activated_at) : null;

        return $this;
    }

    public function normalize()
    {
        $fields  = [ 'id', 'name', 'mobile', 'email', 'nickname', 'portrait', 'gender' ];

        return array_combine(
            $fields,
            array_map(function ($k) {
                return $this->get($k);
            }, $fields)
        );
    }

    private function get($key)
    {
        $f = 'get'.$this->tr($key);
        if (method_exists($this, $f))
            return $this->$f();
    }

    private function set($key, $value)
    {
        $f = 'set'.$this->tr($key);
        if (method_exists($this, $f))
            return $this->$f($value);

        return $this;
    }

    private function tr($key)
    {
        return implode(array_map('ucfirst', explode('_', $key)));
    }
}
