<?php

namespace Mojomaja\Component\Aserv;

class Password
{
    private $password;

    public function __construct($password, $hashed = false)
    {
        $this->password = $hashed || $password instanceof Password ? $password : md5($password);
    }

    public function __toString()
    {
        return (string) $this->password;
    }
}
