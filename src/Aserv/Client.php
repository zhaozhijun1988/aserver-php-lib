<?php

namespace Mojomaja\Component\Aserv;

class Client
{
    const E_OK                  = 0;
    const E_MISSING_ARGUMENT    = 0x81000002;
    const E_BAD_CREDENTIAL      = 0x810003e9;
    const E_FALSE_IDENT         = 0x810003ea;
    const E_FALSE_PASSWORD      = 0x810003eb;
    const E_DUPLICATE_IDENT     = 0x810007d1;
    const E_BAD_TOKEN           = 0x810003f3;
    const E_FIXED_NAME          = 0x810003fd;

    private $gw;
    private $skurl;
    private $token;

    public function __construct($gw, \Mojomaja\Component\Skurl\Client $skurl)
    {
        $this->gw       = $gw;
        $this->skurl    = $skurl;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function revision()
    {
        return $this->weave($this->skurl->get("{$this->gw}/"))['revision'];
    }

    public function register(User $user, $password, $active = true)
    {
        $r = $this->weave($this->skurl->post("{$this->gw}/v1/register", array_filter(
            array_merge(
                $user->normalize(),
                [
                    'password'  => new Password($password),
                    'active'    => json_encode($active)
                ]
            )
        )));
        $this->setToken($r['token']);

        return new User($r['user']);
    }

    public function login($ident, $password)
    {
        $r = $this->weave($this->skurl->post("{$this->gw}/v1/login", [
            'ident'     => $ident,
            'password'  => new Password($password)
        ]));
        $this->setToken($r['token']);

        return new User($r['user']);
    }

    public function logout()
    {
        $this->weave($this->skurl->post("{$this->gw}/v1/logout", [
            'token'     => $this->getToken()
        ]));
    }

    public function profile()
    {
        $r = $this->weave($this->skurl->get("{$this->gw}/v1/profile", [
            'token'     => $this->getToken()
        ]));

        return new User($r['user']);
    }

    public function flush(User $user)
    {
        $r = $this->weave($this->skurl->put("{$this->gw}/v1/profile", array_filter(
            array_merge(
                $user->normalize(),
                [ 'token' => $this->getToken() ]
            )
        )));

        return new User($r['user']);
    }

    public function check($password)
    {
        $this->weave($this->skurl->get("{$this->gw}/v1/password", [
            'token'     => $this->getToken(),
            'password'  => new Password($password)
        ]));
    }

    public function password($new_password, $old_password)
    {
        if ($old_password !== true)
            $this->weave($this->skurl->post("{$this->gw}/v1/password", [
                'token'         => $this->getToken(),
                'new_password'  => new Password($new_password),
                'old_password'  => new Password($old_password)
            ]));
        else
            $this->weave($this->skurl->put("{$this->gw}/v1/password", [
                'token'     => $this->getToken(),
                'password'  => new Password($new_password)
            ]));
    }

    public function su($ident)
    {
        $r = $this->weave($this->skurl->post("{$this->gw}/v1/substitute", ['ident' => $ident]));
        $this->setToken($r['token']);

        return new User($r['user']);
    }

    public function search(Array $ident, $active = true)
    {
        $qs     = array_map(function ($id) { return http_build_query(['ident' => $id]); }, $ident);
        $qs[]   = http_build_query(['active' => json_encode($active)]);
        $r      = $this->weave($this->skurl->get("{$this->gw}/v1/search", implode('&', $qs)));

        return array_map(function ($u) { return new User($u); }, $r['user']);
    }

    public function fetch(Array $id, $active = true)
    {
        $qs     = array_map(function ($id) { return http_build_query(['id' => $id]); }, $id);
        $qs[]   = http_build_query(['active' => json_encode($active)]);
        $r      = $this->weave($this->skurl->get("{$this->gw}/v1/fetch", implode('&', $qs)));

        return array_map(function ($u) { return new User($u); }, $r['user']);
    }

    private function weave($json)
    {
        $result = json_decode($json, true);
        if ($result === null)
            throw new JsonException();

        $result = array_merge(['error' => self::E_OK], $result);
        if ($result['error'] !== self::E_OK)
            throw new Exception($result['message'], $result['error']);

        return $result;
    }
}
